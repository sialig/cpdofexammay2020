package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class Tests {


        @Test
        public void usefullnessDesc() throws Exception {
                        String result1 = new Usefulness().desc();
                assertTrue("About",result1.contains("DevOps is about transformation"));
}
        @Test
        public void functionWFTest() throws Exception {
                Usefulness myobj= new Usefulness();
		myobj.functionWF();
                assertTrue(true);
        }
        @Test
        public void minMaxTest() throws Exception {
                int result2 = new MinMax().f(1,2);
                assertEquals("minmax1",2,result2);
        }

        @Test
        public void minMaxTest2() throws Exception {
                int result3 = new MinMax().f(2,1);
                assertEquals("minmax1",2,result3);
        }
  @Test
        public void minMaxBar1() throws Exception {
                String result = new MinMax().bar("");
                assertEquals("minmaxbar1","",result);
        }

  @Test
        public void duration1() throws Exception {
                String result = new Duration().dur();
                assertTrue(result.contains("CP"));
        }
  @Test
        public void duration2() throws Exception {
                int result = new Duration().calculateIntValue();
                assertEquals("calculateIntValue",result,5);
        }
}
